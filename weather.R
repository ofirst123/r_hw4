setwd("C:/Users/user/Desktop/לימודים/שנה ד/סמסטר ב/R")

read.csv('weatherAUS.csv')
weather.raw<- read.csv('weatherAUS.csv')
str(weather.raw)

summary(weather.raw)

weather.prepared<- weather.raw
#weather.prepared<- weather.prepared[-c(60000:100000),]

#נסדר את התאים הריקים במשתנים הנומרים לממוצע הערכים בעמודה כדי לפגוע כמה שפחות בנתונים
make_average <- function(x,xvec){
  if(is.na(x)) return(mean(xvec, na.rm = T))
  return(x)
}
#נכתוב פונקציה שתטפל בעמודת החודש
make_unknow_num <- function(x){
  if(is.na(x)) return('9999-99-99')
  return(x)
}
make_unknow <- function(x){
  if(is.na(x)) return('unknow')
  return(x)
}

weather.prepared$Date<- as.character(weather.prepared$Date)
weather.prepared$Date <- sapply(weather.prepared$Date,make_unknow_num)
data <- substr(weather.prepared$Date,1,10)
month <- as.integer(substr(data,6,7))
weather.prepared$month<- month

#weather.prepared$MinTemp <- sapply(weather.prepared$MinTemp, make_average,xvec = weather.prepared$MinTemp)
#weather.prepared$MaxTemp <- sapply(weather.prepared$MaxTemp, make_average,xvec = weather.prepared$MaxTemp)
#weather.prepared$Temp3pm <- sapply(weather.prepared$Temp3pm, make_average,xvec = weather.prepared$Temp3pm)
#weather.prepared$Sunshine <- sapply(weather.prepared$Sunshine, make_average,xvec = weather.prepared$Sunshine)
#weather.prepared$Evaporation <- sapply(weather.prepared$Evaporation, make_average,xvec = weather.prepared$Evaporation)
#weather.prepared$Cloud3pm <- sapply(weather.prepared$Cloud3pm, make_average,xvec = weather.prepared$Cloud3pm)
#weather.prepared$Cloud9am <- sapply(weather.prepared$Cloud9am, make_average,xvec = weather.prepared$Cloud9am)

weather.prepared$WindGustDir <- as.character(weather.prepared$WindGustDir)
weather.prepared$WindGustDir <- sapply(weather.prepared$WindGustDir, make_unknow)
weather.prepared$WindGustDir<- as.factor(weather.prepared$WindGustDir)



#EDA(explorer data analisies)

#library(ggplot2)

#ggplot(weather.prepared, aes(WindGustDir)) + geom_bar(aes(fill = RainTomorrow))
#ggplot(weather.prepared, aes(WindDir9am)) + geom_bar(aes(fill = RainTomorrow))
#ggplot(weather.prepared, aes(WindDir3pm)) + geom_bar(aes(fill = RainTomorrow))
#ggplot(weather.prepared, aes(WindDir9am)) + geom_bar(aes(fill = RainTomorrow))


#weather.prepared$WindSpeed9am<- NULL
#weather.prepared$WindSpeed3pm<- NULL
#weather.prepared$Pressure9am<- NULL
#weather.prepared$Pressure3pm<- NULL
#weather.prepared$WindGustSpeed<- NULL
#weather.prepared$Evaporation<- NULL
#weather.prepared$WindDir9am<- NULL
#weather.prepared$WindDir3pm<- NULL

#weather.prepared$Temp9am<- NULL
#weather.prepared$Temp3pm<- NULL
#weather.prepared$Sunshine<- NULL
#weather.prepared$Humidity3pm<- NULL
weather.prepared$Date<- NULL

summary(weather.prepared)
str(weather.prepared)

#מחלקים את הנתונים לבדיקה ואימון
library('caTools')

filter<- sample.split(weather.prepared$Location, SplitRatio = 0.7)
weather.train <- subset(weather.prepared, filter = T)
weather.test <- subset(weather.prepared, filter = F)


weather.model<- glm(RainTomorrow~., family = binomial(link = 'logit'), data = weather.train)
summary(weather.model)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     

str(weather.prepared)

#step.model.income <- step(weather.model)

#summary(step.model.income)


#הרמת המודל על נתוני הבדיקה
predict.weather.test <- predict(weather.model,newdata = weather.test, type = 'response')

confusion.matrix <- table(predict.weather.test>0.5, weather.test$RainTomorrow)

precision<-  confusion.matrix[2,2]/(confusion.matrix[2,1]+confusion.matrix[2,2])

recall<- confusion.matrix[2,2]/(confusion.matrix[1,2]+confusion.matrix[2,2])

summary(weather.model)
